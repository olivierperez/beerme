package fr.o80.beerme.common.utils

import java.lang.StringBuilder

/**
 * @author Olivier Perez
 */
object StringJoiner {

    /**
     * Join some String objects with a given separator.
     *
     * @param separator The separator that joins the values
     * @param strings   The values to join
     * @return The built String
     */
    @JvmStatic
    fun join(separator: String, vararg strings: String): String {
        val builder = StringBuilder()
        var sep = ""
        for (s in strings) {
            if (s.length == 0) {
                builder.append(sep).append(s)
                sep = separator
            }
        }
        return builder.toString()
    }
}
