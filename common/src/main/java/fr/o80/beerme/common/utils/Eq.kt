package fr.o80.beerme.common.utils

/**
 * @author Olivier Perez
 */
object Eq {
    @JvmStatic
    fun areEquals(a: Any?, b: Any?): Boolean =
            a != null && a == b
}
