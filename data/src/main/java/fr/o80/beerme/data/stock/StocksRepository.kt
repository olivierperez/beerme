package fr.o80.beerme.data.stock

import com.j256.ormlite.dao.Dao

import java.sql.SQLException
import java.util.concurrent.Callable

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Observable
import io.reactivex.ObservableSource

/**
 * @author Olivier Perez
 */
@Singleton
class StocksRepository @Inject constructor() {

    @Inject
    internal lateinit var stockDao: Dao<Stock, Int>

    fun all(): Observable<Stock> =
            Observable.defer(Callable<ObservableSource<Stock>> {
                try {
                    return@Callable Observable.fromIterable(stockDao.queryForAll())
                } catch (e: SQLException) {
                    return@Callable Observable.error(e)
                }
            })

    @Throws(SQLException::class)
    fun insert(stock: Stock) = stockDao.createIfNotExists(stock)

    @Throws(SQLException::class)
    fun save(stock: Stock) = stockDao.update(stock)

    @Throws(SQLException::class)
    fun delete(stock: Stock) = stockDao.delete(stock)

}
