package fr.o80.beerme.data.diff

import android.support.v7.util.DiffUtil

/**
 * @author Olivier Perez
 */
class DiffCallback(private val oldItems: List<Diffable>?, private val newItems: List<Diffable>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldItems?.size ?: 0

    override fun getNewListSize() = newItems.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems!![oldItemPosition].id == newItems[newItemPosition].id

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems!![oldItemPosition] == newItems[newItemPosition]
}
