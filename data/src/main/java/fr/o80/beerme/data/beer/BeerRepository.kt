package fr.o80.beerme.data.beer

import com.j256.ormlite.dao.Dao

import java.sql.SQLException
import java.util.concurrent.Callable

import javax.inject.Inject
import javax.inject.Singleton

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.SingleSource
import io.reactivex.schedulers.Schedulers

/**
 * @author Olivier Perez
 */
@Singleton
class BeerRepository @Inject
constructor(private val beerDao: Dao<Beer, Int>) {

    fun all(): Single<MutableList<Beer>> =
            Single
                    .fromCallable { beerDao.queryForAll() }
                    .subscribeOn(Schedulers.io())

    fun insert(beer: Beer): Single<Beer> =
            Single.defer {
                try {
                    Single.just(beerDao.createIfNotExists(beer))
                } catch (e: SQLException) {
                    Single.error<Beer>(e)
                }
            }

    fun save(beer: Beer): Single<Boolean> =
            Single.defer {
                try {
                    val rowsUpdated = beerDao.update(beer)
                    Single.just(rowsUpdated == 1)
                } catch (e: SQLException) {
                    Single.error<Boolean>(e)
                }
            }

    fun delete(beer: Beer): Completable =
            Completable.fromAction { beerDao.delete(beer) }

}
