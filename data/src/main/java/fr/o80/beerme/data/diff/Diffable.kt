package fr.o80.beerme.data.diff

/**
 * @author Olivier Perez
 */
interface Diffable {
    val id: Int
}
