package fr.o80.beerme.data.stock;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.parceler.Parcel;

import java.util.Date;

import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.diff.Diffable;

import static fr.o80.beerme.common.utils.Eq.areEquals;

/**
 * @author Olivier Perez
 */
@Parcel
@DatabaseTable(tableName = "stock")
public class Stock implements Diffable {

    @DatabaseField(columnName = "id", generatedId = true)
    int id;

    @DatabaseField(columnName = "beer", foreign = true, foreignAutoRefresh = true)
    Beer beer;

    @DatabaseField(columnName = "quantity")
    int quantity;

    @DatabaseField(columnName = "expiry")
    Date expiry;

    @DatabaseField(columnName = "volume")
    String volume;

    public Stock() {
    }

    public Stock(final Beer beer, final int quantity, final Date expiry, final String volume) {
        this.expiry = expiry;
        this.beer = beer;
        this.quantity = quantity;
        this.volume = volume;
    }

    @Override
    public int getId() {
        return id;
    }

    public Beer getBeer() {
        return beer;
    }

    public int getQuantity() {
        return quantity;
    }

    public Date getExpiry() {
        return expiry;
    }

    public String getVolume() {
        return volume;
    }

    public void setBeer(final Beer beer) {
        this.beer = beer;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    public void setExpiry(final Date expiry) {
        this.expiry = expiry;
    }

    public void setVolume(final String volume) {
        this.volume = volume;
    }

    public boolean isExpired() {
        return expiry.before(new Date());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Stock)) {
            return false;
        }

        Stock o = (Stock) obj;
        return id == o.id
                && areEquals(beer, o.beer)
                && quantity == o.quantity
                && areEquals(expiry, o.expiry)
                && areEquals(volume, o.volume);
    }
}
