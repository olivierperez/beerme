package fr.o80.beerme.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;

import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.stock.Stock;
import timber.log.Timber;

/**
 * @author Olivier Perez
 */
@Singleton
public class BeerMeDB extends OrmLiteSqliteOpenHelper {

    private static final String BEER_DATABASE = "BEER_DATABASE";
    private static final int DATABASE_VERSION = 3;

    private static final String VOLUME_50_CL = "50cl";
    private static final String VOLUME_75_CL = "75cl";

    @Inject
    public BeerMeDB(final Context context) {
        super(context, BEER_DATABASE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase database, final ConnectionSource connectionSource) {
        Timber.i("Create database...");

        try {
            TableUtils.createTable(connectionSource, Beer.class);
            TableUtils.createTable(connectionSource, Stock.class);

            Beer b1614 = new Beer("1664", 2f, "Blanche", "Simple");
            Beer leffe = new Beer("Leffe Ruby", 3.5f, "Blonde", "Aromatisée");
            Beer heineken = new Beer("Heineken", 1.5f, "Blanche", "Triple");
            Beer tsingtao = new Beer("Tsingtao", 2f, "Brune", "Simple");

            Dao<Beer, Integer> beerDao = getBeersDao();
            beerDao.create(b1614);
            beerDao.create(leffe);
            beerDao.create(heineken);
            beerDao.create(tsingtao);

            Dao<Stock, Integer> purchaseDao = getStockDao();
            Date expiry = new Date();
            purchaseDao.create(new Stock(leffe, 3, expiry, VOLUME_50_CL));
            purchaseDao.create(new Stock(heineken, 6, expiry, VOLUME_75_CL));

            Timber.d("Create database...");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final ConnectionSource connectionSource, final int oldVersion, final int newVersion) {
        try {
            if (oldVersion < 2) {
                updateToV2();
            }
            if (oldVersion < 3) {
                updateToV3();
            }
        } catch (SQLException e) {
            Timber.e(e, "Failed to update database");
            throw new RuntimeException(e);
        }
    }

    /**
     * Update to V2
     */
    private void updateToV2() throws SQLException {
        // Rename column of "stock"
        getStockDao().executeRaw("ALTER TABLE `stock` RENAME TO old_stock");
        TableUtils.createTable(connectionSource, Stock.class);
        getStockDao().executeRaw("INSERT INTO stock (id, beer, quantity, expiry, volume) SELECT mId, mBeer_id, mQuantity, mExpiry, mVolume FROM old_stock");
        getStockDao().executeRaw("DROP TABLE old_stock");

        // Rename column of "beer"
        getBeersDao().executeRaw("ALTER TABLE `beer` RENAME TO old_beer");
        TableUtils.createTable(connectionSource, Beer.class);
        getBeersDao().executeRaw("INSERT INTO beer (id, name, brewer, color, type, rate, comment, toBuy) SELECT mId, name, brewer, color, type, rate, comment, toBuy FROM old_beer");
        getBeersDao().executeRaw("DROP TABLE old_beer");
    }

    /**
     * Update to V3
     */
    private void updateToV3() throws SQLException {
        // rate was an int from 0 to 10, it is now a float from 0 to 5. So we need to divide it by 2
        getBeersDao().executeRaw("ALTER TABLE `beer` RENAME TO old_beer");
        TableUtils.createTable(connectionSource, Beer.class);
        getBeersDao().executeRaw("INSERT INTO beer (id, name, brewer, color, type, rate, comment, toBuy) SELECT id, name, brewer, color, type, rate/2.0, comment, toBuy FROM old_beer");
        getBeersDao().executeRaw("DROP TABLE old_beer");
    }

    public Dao<Beer, Integer> getBeersDao() {
        try {
            return getDao(Beer.class);
        } catch (SQLException e) {
            Timber.e(e, "Error while accessing BeerDao");
            return null;
        }
    }

    public Dao<Stock, Integer> getStockDao() {
        try {
            return getDao(Stock.class);
        } catch (SQLException e) {
            Timber.e(e, "Error while accessing StockDao");
            return null;
        }
    }
}
