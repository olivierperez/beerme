package fr.o80.beerme.data.beer;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import org.parceler.Parcel;

import fr.o80.beerme.data.diff.Diffable;

import static fr.o80.beerme.common.utils.Eq.areEquals;

/**
 * @author Olivier Perez
 */
@Parcel
@DatabaseTable(tableName = "beer")
public class Beer implements Diffable {

    @DatabaseField(columnName = "id", generatedId = true)
    int id;

    @DatabaseField(columnName = "name")
    String name;

    @DatabaseField(columnName = "brewer")
    String brewer;

    @DatabaseField(columnName = "color")
    String color;

    @DatabaseField(columnName = "type")
    String type;

    @DatabaseField(columnName = "rate")
    float rate;

    @DatabaseField(columnName = "comment")
    String comment;

    @DatabaseField(columnName = "toBuy")
    boolean toBuy;

    public Beer() {
    }

    public Beer(String name, float rate, String color, String type) {
        this.name = name;
        this.rate = rate;
        this.color = color;
        this.type = type;
    }

    public Beer(String name, String brewer, String color, String type, float rate, String comment) {
        this.name = name;
        this.brewer = brewer;
        this.rate = rate;
        this.color = color;
        this.type = type;
        this.comment = comment;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getBrewer() {
        return brewer;
    }

    public void setBrewer(final String brewer) {
        this.brewer = brewer;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(final String comment) {
        this.comment = comment;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(final float rate) {
        this.rate = rate;
    }

    public boolean isToBuy() {
        return toBuy;
    }

    public void setToBuy(final boolean toBuy) {
        this.toBuy = toBuy;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Beer)) {
            return false;
        }

        Beer o = (Beer) obj;
        return id == o.id
                && areEquals(name, o.name)
                && areEquals(brewer, o.brewer)
                && areEquals(color, o.color)
                && areEquals(type, o.type)
                && rate == o.rate
                && areEquals(comment, o.comment)
                && toBuy == o.toBuy;
    }

    public static Beer create(String name, String brewer, String color, String type, float rate, String comment) {
        return new Beer(name, brewer, color, type, rate, comment);
    }
}
