package fr.o80.beerme.service;

import org.junit.Test;
import org.mockito.InjectMocks;

import fr.o80.beerme.BaseUnitTest;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author Olivier Perez
 */
public class QueryFilterUnitTest extends BaseUnitTest {

    @InjectMocks
    private QueryFilter queryFilter;

    @Test
    public void should_match() {
        assertThat(queryFilter.matches("tsing", "Tsingtao"), is(true));
        assertThat(queryFilter.matches("b", "Brune"), is(true));
        assertThat(queryFilter.matches("b", "Blonde"), is(true));
        assertThat(queryFilter.matches("papa", "Bière du Papa"), is(true));
        assertThat(queryFilter.matches("du papa", "Bière du Papa"), is(true));
        assertThat(queryFilter.matches("Bière du Blonde", "Bière du Papa Blonde"), is(true));
    }

    @Test
    public void should_not_match() {
        assertThat(queryFilter.matches("tao", "Tsingtao"), is(false));
        assertThat(queryFilter.matches("Brunelle", "Brune"), is(false));
        assertThat(queryFilter.matches("Triple", "Blonde"), is(false));
        assertThat(queryFilter.matches("de", "Bière du Papa"), is(false));
        assertThat(queryFilter.matches("Bière du blonde", "Bière du Papa brune"), is(false));
    }
}