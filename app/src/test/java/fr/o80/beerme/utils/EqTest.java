package fr.o80.beerme.utils;

import org.junit.Test;

import fr.o80.beerme.BaseUnitTest;
import fr.o80.beerme.common.utils.Eq;

import static fr.o80.beerme.common.utils.Eq.areEquals;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author Olivier Perez
 */
public class EqTest extends BaseUnitTest {

    @Test
    public void simple_values() {
        assertThat(Eq.areEquals("a", "a"), is(true));
        assertThat(Eq.areEquals(null, null), is(true));
        assertThat(Eq.areEquals("a", "b"), is(false));
        assertThat(Eq.areEquals("a", null), is(false));
        assertThat(Eq.areEquals(null, "b"), is(false));
    }

    @Test
    public void object_values() {
        assertThat(Eq.areEquals(new Inner("a", "b", 0), new Inner("a", "b", 0)), is(true));
        assertThat(Eq.areEquals(new Inner("a", "b", 0), new Inner("a", "b", 666)), is(false));
        assertThat(Eq.areEquals(new Inner("a", "b", 0), new Inner("a", "C", 0)), is(false));
        assertThat(Eq.areEquals(new Inner("a", "b", 0), new Inner("D", "b", 0)), is(false));

    }

    private class Inner {
        private final String a;
        private final String b;
        private final int i;

        private Inner(String a, String b, int i) {
            this.a = a;
            this.b = b;
            this.i = i;
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Inner)) {
                return false;
            }

            Inner o = (Inner) obj;
            return i == o.i
                    && areEquals(a, o.a)
                    && areEquals(b, o.b);
        }
    }
}