package fr.o80.beerme;

import timber.log.Timber;

/**
 * @author Olivier Perez
 */

public class BeerApplication extends BaseApplication {

    @Override
    protected void initTimber() {
        Timber.plant(new Timber.DebugTree());
    }
}
