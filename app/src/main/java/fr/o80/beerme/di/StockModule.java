package fr.o80.beerme.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.o80.beerme.service.UserPrefs;
import fr.o80.beerme.usecase.SoonOutdatedFilter;

/**
 * @author Olivier Perez
 */
@Module
public class StockModule {

    @Provides
    @Singleton
    public SoonOutdatedFilter provideSoonOutdatedFilter(UserPrefs userPrefs) {
        int field = userPrefs.getSoonOutdatedField();
        int thresholdDays = userPrefs.getSoonOutdatedLimitInDays();

        return new SoonOutdatedFilter(field, thresholdDays);
    }
}
