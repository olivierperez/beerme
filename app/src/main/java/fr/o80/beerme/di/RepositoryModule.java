package fr.o80.beerme.di;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fr.o80.beerme.data.BeerMeDB;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.stock.Stock;

/**
 * @author Olivier Perez
 */
@Module
public class RepositoryModule {

    private final Context mContext;

    public RepositoryModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    Dao<Stock, Integer> provideStockDao(BeerMeDB db) {
        return db.getStockDao();
    }

    @Provides
    @Singleton
    Dao<Beer, Integer> provideBeerDao(BeerMeDB db) {
        return db.getBeersDao();
    }

}
