package fr.o80.beerme.di;

import javax.inject.Singleton;

import dagger.Component;
import fr.o80.beerme.presentation.home.view.HomeActivity;
import fr.o80.beerme.presentation.mystock.view.EditStockDialogFragment;
import fr.o80.beerme.presentation.newpurchase.view.NewBeerFragment;
import fr.o80.beerme.presentation.newpurchase.view.NewStockFragment;
import fr.o80.beerme.presentation.newpurchase.view.SelectBeerFragment;
import fr.o80.beerme.presentation.mystock.view.StocksFragment;
import fr.o80.beerme.presentation.reference.view.EditReferenceFragment;
import fr.o80.beerme.service.SoonOutdatedReceiver;
import fr.o80.beerme.service.OnStartupReceiver;

/**
 * @author Olivier Perez
 */
@Singleton
@Component(modules = {RepositoryModule.class, StockModule.class})
public interface AppComponent {
    // Activities
    void inject(HomeActivity homeActivity);

    // Fragments
    void inject(StocksFragment fragment);
    void inject(SelectBeerFragment fragment);
    void inject(NewStockFragment fragment);
    void inject(EditStockDialogFragment fragment);
    void inject(NewBeerFragment fragment);
    void inject(EditReferenceFragment fragment);

    // Receivers
    void inject(OnStartupReceiver receiver);
    void inject(SoonOutdatedReceiver receiver);
}
