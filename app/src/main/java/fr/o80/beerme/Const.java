package fr.o80.beerme;

/**
 * @author Olivier Perez
 */
public final class Const {

    public static final int REQ_CODE_SOON_OUTDATED_NOTIFICATION = 0;
    public static final int REQ_CODE_SOON_OUTDATED_ALARM = 1;
    public static final long SHORT_DELAY = 25;

    private Const() {
    }
}
