package fr.o80.beerme.presentation.behavior;

import android.animation.ValueAnimator;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

/**
 * @author Olivier Perez
 */
class SnackBarBehavior extends CoordinatorLayout.Behavior<Toolbar> {

    private static final float A_FLOAT = 0.667f;

    private ValueAnimator mFabTranslationYAnimator;
    private float mToolbarTranslationY;

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent,
                                   Toolbar child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, Toolbar child, View dependency) {
        if (dependency instanceof Snackbar.SnackbarLayout) {
            updateFabTranslationForSnackbar(parent, child);
        }
        return false;
    }

    void updateFabTranslationForSnackbar(CoordinatorLayout parent, final Toolbar toolbar) {
        final float targetTransY = getFabTranslationYForSnackbar(parent, toolbar);
        if (mToolbarTranslationY == targetTransY) {
            // We're already at (or currently animating to) the target value, return...
            return;
        }

        final float currentTransY = ViewCompat.getTranslationY(toolbar);

        // Make sure that any current animation is cancelled
        if (mFabTranslationYAnimator != null && mFabTranslationYAnimator.isRunning()) {
            mFabTranslationYAnimator.cancel();
        }

        if (toolbar.isShown()
              && Math.abs(currentTransY - targetTransY) > (toolbar.getHeight() * A_FLOAT)) {
            // If the FAB will be travelling by more than 2/3 of it's height, let's animate
            // it instead
            if (mFabTranslationYAnimator == null) {
                mFabTranslationYAnimator = ValueAnimator.ofFloat(0, 1);
                mFabTranslationYAnimator.setInterpolator(new FastOutSlowInInterpolator());
                mFabTranslationYAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(final ValueAnimator animation) {
                        ViewCompat.setTranslationY(toolbar, (Float) animation.getAnimatedValue());
                    }
                });
            }
            mFabTranslationYAnimator.setFloatValues(currentTransY, targetTransY);
            mFabTranslationYAnimator.start();
        } else {
            // Now update the translation Y
            ViewCompat.setTranslationY(toolbar, targetTransY);
        }

        mToolbarTranslationY = targetTransY;
    }

    private float getFabTranslationYForSnackbar(CoordinatorLayout parent, Toolbar fab) {
        float minOffset = 0;
        final List<View> dependencies = parent.getDependencies(fab);
        for (int i = 0, z = dependencies.size(); i < z; i++) {
            final View view = dependencies.get(i);
            if (view instanceof Snackbar.SnackbarLayout && parent.doViewsOverlap(fab, view)) {
                minOffset = Math.min(minOffset,
                      ViewCompat.getTranslationY(view) - view.getHeight());
            }
        }

        return minOffset;
    }

    @Override
    public boolean onLayoutChild(CoordinatorLayout parent, Toolbar child, int layoutDirection) {
        // Now let the CoordinatorLayout lay out the FAB
        parent.onLayoutChild(child, layoutDirection);
        return true;
    }
}