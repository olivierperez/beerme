package fr.o80.beerme.presentation.reference.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.BaseFragment;
import fr.o80.beerme.presentation.common.ColorAdapter;
import fr.o80.beerme.presentation.common.TypeAdapter;
import fr.o80.beerme.presentation.reference.presenter.EditReferencePresenter;
import fr.o80.beerme.presentation.reference.presenter.EditReferencePresenterView;

/**
 * @author Olivier Perez
 */
public class EditReferenceFragment extends BaseFragment<EditReferencePresenter> implements EditReferencePresenterView {

    private static final String EXTRA_BEER = "EXTRA_BEER";

    @Inject
    protected EditReferencePresenter presenter;

    @BindView(R.id.beer_name)
    protected EditText name;

    @BindView(R.id.beer_brewer)
    protected EditText brewer;

    @BindView(R.id.beer_color)
    protected MaterialBetterSpinner color;

    @BindView(R.id.beer_type)
    protected MaterialBetterSpinner type;

    @BindView(R.id.rating_bar)
    protected SimpleRatingBar ratingBar;

    @BindView(R.id.beer_comment)
    protected EditText comment;

    @BindView(R.id.beer_sumbit)
    protected Button submitBtn;

    private Beer beer;
    private boolean editing;
    private EditReferenceListener editReferenceListener;

    public static EditReferenceFragment editBeer(Beer beer) {
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_BEER, Parcels.wrap(beer));

        EditReferenceFragment fragment = new EditReferenceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static EditReferenceFragment newBeer() {
        return new EditReferenceFragment();
    }

    @Override
    protected EditReferencePresenter presenter() {
        return presenter;
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_edit_beer;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof EditReferenceListener) {
            editReferenceListener = (EditReferenceListener) context;
        } else {
            throw new RuntimeException("Context must be a NewStockListener");
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        component().inject(this);

        if (getArguments() != null && getArguments().containsKey(EXTRA_BEER)) {
            beer = Parcels.unwrap(getArguments().getParcelable(EXTRA_BEER));
            editing = true;
        } else {
            editing = false;
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (editing) {
            name.setText(beer.getName());
            brewer.setText(beer.getBrewer());
            color.setText(beer.getColor());
            type.setText(beer.getType());
            ratingBar.setRating(beer.getRate());
            comment.setText(beer.getComment());
            submitBtn.setText(R.string.common_validate);
        }

        // Colors
        String[] colors = getResources().getStringArray(R.array.beer_colors);
        color.setAdapter(new ColorAdapter(getActivity(), colors));

        // Types
        String[] types = getResources().getStringArray(R.array.beer_types);
        type.setAdapter(new TypeAdapter(getActivity(), types));
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(R.string.references_title);
    }

    @OnClick(R.id.beer_sumbit)
    public void onSubmitClicked() {
        presenter.save(
                editing ? beer : new Beer(),
                name.getText().toString(),
                brewer.getText().toString(),
                color.getText().toString(),
                type.getText().toString(),
                ratingBar.getRating(),
                comment.getText().toString()
        );
    }

    @Override
    public void clearErrors() {
        name.setError(null);
    }

    @Override
    public void errorOnName(@StringRes int strRes) {
        name.setError(getString(strRes));
    }

    @Override
    public void onBeerSaved(Beer beer) {
        editReferenceListener.onBeeerSaved(beer);
    }

    public interface EditReferenceListener {
        void onBeeerSaved(Beer beer);
    }
}
