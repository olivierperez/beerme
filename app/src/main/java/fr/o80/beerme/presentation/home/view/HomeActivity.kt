package fr.o80.beerme.presentation.home.view

import android.app.PendingIntent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.widget.Toast
import fr.o80.beerme.BeerApplication
import fr.o80.beerme.R
import fr.o80.beerme.presentation.about.AboutActivity
import fr.o80.beerme.presentation.base.BaseActivity
import fr.o80.beerme.presentation.mystock.view.MyStockActivity
import fr.o80.beerme.presentation.newpurchase.NewPurchaseActivity
import fr.o80.beerme.presentation.reference.ReferencesActivity
import fr.o80.beerme.service.AlarmService
import fr.o80.beerme.service.SoonOutdatedReceiver
import fr.o80.beerme.usecase.StockList
import kotlinx.android.synthetic.main.activity_recyclerview.*
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

/**
 * @author Olivier Perez
 */
class HomeActivity : BaseActivity(), SectionAdapter.Listener {

    @Inject
    lateinit var alarmService: AlarmService

    private val sections = ArrayList<SectionAdapter.Section>(5)

    init {
        sections.add(SectionAdapter.Section(R.string.section_soon_outdated, R.drawable.ic_beer) {
            startActivity(MyStockActivity.newIntent(this, StockList.SORT_BY_EXPIRY))
        })
        sections.add(SectionAdapter.Section(R.string.section_stock, R.drawable.ic_stock) {
            startActivity(MyStockActivity.newIntent(this, StockList.SORT_BY_BEER))
        })
        sections.add(SectionAdapter.Section(R.string.section_references, R.drawable.ic_reference) {
            startActivity(ReferencesActivity.newIntent(this))
        })
        sections.add(SectionAdapter.Section(R.string.section_newpurchase, R.drawable.ic_purchase) {
            startActivity(NewPurchaseActivity.newIntent(this))
        })
        sections.add(SectionAdapter.Section(R.string.section_info, R.drawable.ic_info) {
            startActivity(AboutActivity.newIntent(this))
        })
    }

    override fun layoutId(): Int {
        return R.layout.activity_recyclerview
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as BeerApplication).component().inject(this)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = SectionAdapter(sections, this)

        scheduleAlarm()
    }

    private fun scheduleAlarm() {
        val alarmStopped = SoonOutdatedReceiver.newInstance(this, PendingIntent.FLAG_NO_CREATE) == null
        if (alarmStopped) {
            Timber.d("Need to start alarm")
            alarmService.startAlarm(this)
        }
    }

    override fun onSectionClicked(section: SectionAdapter.Section) {
        val title = getString(section.title)
        Timber.d("Section clicked: %s", title)

        if (section.open != null) {
            section.open.invoke()
        } else {
            Toast.makeText(this, "La fonctionnalité \"$title\" n'est pas encore disponible", Toast.LENGTH_SHORT).show()
        }
    }
}
