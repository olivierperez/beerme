package fr.o80.beerme.presentation.newpurchase

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import fr.o80.beerme.R
import fr.o80.beerme.data.beer.Beer
import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.presentation.base.BaseActivity
import fr.o80.beerme.presentation.newpurchase.view.NewBeerFragment
import fr.o80.beerme.presentation.newpurchase.view.NewStockFragment
import fr.o80.beerme.presentation.newpurchase.view.SelectBeerFragment
import org.parceler.Parcels

/**
 * @author Olivier Perez
 */
class NewPurchaseActivity : BaseActivity(), SelectBeerFragment.SelectBeerListener, NewStockFragment.NewStockListener, NewBeerFragment.NewBeerListener {

    override fun layoutId(): Int {
        return R.layout.activity_simple
    }

    /* Fragment */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fragment = supportFragmentManager.findFragmentById(R.id.main)

        if (fragment == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main, SelectBeerFragment.newInstance(R.string.section_newpurchase), SelectBeerFragment.TAG)
                    .commit()
        }
    }

    /* SelectBeerListener */

    override fun onBeerClicked(beer: Beer) {
        replaceFragmentWithAnimation(NewStockFragment.newInstance(beer), true)
    }

    override fun onNewBeerClicked() {
        replaceFragmentWithAnimation(NewBeerFragment.newInstance(), true)
    }

    /* NewStockListener  */

    override fun onStockAdded(stock: Stock) {
        val data = Intent()
        data.putExtra(EXTRA_STOCK, Parcels.wrap(stock))
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    /* NewBeerListener */

    override fun onNewBeer(beer: Beer) {
        replaceFragmentWithAnimation(NewStockFragment.newInstance(beer), false)
    }

    companion object {
        const val EXTRA_STOCK = "EXTRA_STOCK"
        fun newIntent(context: Context) = Intent(context, NewPurchaseActivity::class.java)
    }
}
