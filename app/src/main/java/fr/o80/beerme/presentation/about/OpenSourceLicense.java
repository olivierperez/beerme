package fr.o80.beerme.presentation.about;

import fr.o80.beerme.R;

/**
 * @author Olivier Perez
 */
public enum OpenSourceLicense {
    APACHE_2, MIT, GNU_GPL_3, ISC;

    public int getResourceId() {
        switch (this) {
            case APACHE_2:
                return R.string.license_apache2;
            case MIT:
                return R.string.license_mit;
            case GNU_GPL_3:
                return R.string.license_gpl;
            case ISC:
                return R.string.license_isc;
            default:
                return -1;
        }
    }
}
