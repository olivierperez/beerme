package fr.o80.beerme.presentation.newpurchase.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.BaseFragment;
import fr.o80.beerme.presentation.common.BeerAdapter;
import fr.o80.beerme.presentation.newpurchase.presenter.SelectBeerPresenter;
import fr.o80.beerme.presentation.newpurchase.presenter.SelectBeerView;
import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * @author Olivier Perez
 */
public class SelectBeerFragment extends BaseFragment<SelectBeerPresenter> implements SelectBeerView, BeerAdapter.BeerAdapterListener {

    public static final String TAG = SelectBeerFragment.class.getSimpleName();

    private static final String EXTRA_TITLE = "EXTRA_TITLE";

    @Inject
    protected SelectBeerPresenter presenter;

    @BindView(R.id.recylerview)
    protected RecyclerView recyclerView;

    @BindView(R.id.fab)
    protected FloatingActionButton fab;

    private BeerAdapter adapter;

    private SelectBeerListener selectBeerListener;

    public static SelectBeerFragment newInstance(@StringRes int titleStrRes) {

        Bundle args = new Bundle();
        args.putInt(EXTRA_TITLE, titleStrRes);

        SelectBeerFragment fragment = new SelectBeerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /* Fragment */

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof SelectBeerListener) {
            selectBeerListener = (SelectBeerListener) context;
        } else {
            throw new RuntimeException("Context must be a SelectBeerListener");
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        component().inject(this);

        adapter = new BeerAdapter(this);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new FadeInLeftAnimator(new DecelerateInterpolator(5f)));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectBeerListener.onNewBeerClicked();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(getArguments().getInt(EXTRA_TITLE)));

        presenter.init();
    }

    /* BaseFragment */

    @Override
    protected SelectBeerPresenter presenter() {
        return presenter;
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_select_beer;
    }

    /* SelectBeerView */

    @Override
    public void updateBeers(final List<Beer> beers) {
        adapter.addAll(beers);
    }

    @Override
    public void onItemClicked(Beer beer) {
        selectBeerListener.onBeerClicked(beer);
    }

    @Override
    public void onDelete(Beer beer) {
        presenter.onDelete(beer);
    }

    public interface SelectBeerListener {
        void onBeerClicked(Beer beer);
        void onNewBeerClicked();
    }
}
