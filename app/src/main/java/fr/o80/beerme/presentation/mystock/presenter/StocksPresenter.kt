package fr.o80.beerme.presentation.mystock.presenter

import fr.o80.beerme.R
import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.presentation.base.Presenter
import fr.o80.beerme.presentation.common.StockAdapter
import fr.o80.beerme.usecase.StockList
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * @author Olivier Perez
 */
class StocksPresenter @Inject constructor(var stockList: StockList) : Presenter<StocksView>(), StockAdapter.StockListListener {

    private var stocks: List<Stock>? = null

    private var sortType = StockList.SORT_BY_BEER

    override fun attach(v: StocksView) {
        super.attach(v)
        v.setTitle(R.string.app_name)

        disposables.add(v.searcher()
                                .debounce(750, TimeUnit.MILLISECONDS)
                                .switchMap { query ->
                                    Timber.i("Query: %s", query)
                                    Observable.fromIterable(stocks!!)
                                            .subscribeOn(Schedulers.computation())
                                            .filter(stockList.filter(query))
                                }
                                .toList()
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeBy(
                                        onSuccess = { stocks ->
                                            Timber.d("Matching stock found")
                                            view?.let {
                                                if (stocks.isEmpty()) {
                                                    view.showEmptyMessage(R.string.stock_no_result)
                                                } else {
                                                    view.items(stocks)
                                                }
                                            }
                                        },

                                        onError = { e ->
                                            Timber.e(e, "Error while searching in stock")
                                        }))
    }

    fun load(sort: String) {
        Timber.d("Load stocks by %s", sort)
        sortType = sort

        if (view == null) {
            Timber.w("View is not attached!")
            return
        }

        disposables.add(stockList.all(sortType)
                                .doOnSuccess { stocks = it }
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeBy(
                                        onSuccess = { stocks ->
                                            Timber.d("Stock loaded")
                                            view?.let {
                                                if (stocks.isEmpty()) {
                                                    view.showEmptyMessage(R.string.stock_empty)
                                                } else {
                                                    view.items(stocks)
                                                }
                                            }
                                        },
                                        onError = { e ->
                                            Timber.e(e, "Error while filtering stock")
                                        }))
    }

    override fun onStockClicked(stock: Stock) {
        Timber.i("A stock is clicked: %s", stock.beer.name)
        view.openEditStock(stock)
    }

    fun onSortClicked() {
        Timber.d("Sort clicked")
        view.showSortOptions(intArrayOf(R.string.by_name, R.string.by_expiry), if (sortType == StockList.SORT_BY_BEER) 0 else 1)
    }

    fun onSortSelected(which: Int) {
        load(if (which == 0) StockList.SORT_BY_BEER else StockList.SORT_BY_EXPIRY)
    }

}
