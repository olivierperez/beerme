package fr.o80.beerme.presentation.newpurchase.presenter;

import javax.inject.Inject;

import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.beer.BeerRepository;
import fr.o80.beerme.presentation.base.Presenter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 */
public class NewBeerPresenter extends Presenter<NewBeerView> {

    @Inject
    BeerRepository beerRepository;

    @Inject
    public NewBeerPresenter() {
    }

    public void addBeer(String name, String brewer, String color, String type, float rate, String comment) {
        boolean error = false;
        view.clearErrors();

        if (name.isEmpty()) {
            view.errorOnName(R.string.new_beer_error_missing_name);
            error = true;
        }

        if (!error) {
            disposables.add(beerRepository.insert(Beer.create(name, brewer, color, type, rate, comment))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Beer>() {
                        @Override
                        public void onSuccess(Beer beer) {
                            view.goToStock(beer);
                        }

                        @Override
                        public void onError(Throwable error) {
                            view.showError(0, R.string.error_cannot_save_beer);
                        }
                    }));
        }
    }
}
