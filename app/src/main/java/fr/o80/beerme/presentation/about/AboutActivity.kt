package fr.o80.beerme.presentation.about

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import com.danielstone.materialaboutlibrary.ConvenienceBuilder
import com.danielstone.materialaboutlibrary.MaterialAboutActivity
import com.danielstone.materialaboutlibrary.items.MaterialAboutActionItem
import com.danielstone.materialaboutlibrary.model.MaterialAboutCard
import com.danielstone.materialaboutlibrary.model.MaterialAboutList
import com.mikepenz.community_material_typeface_library.CommunityMaterial
import com.mikepenz.google_material_typeface_library.GoogleMaterial
import com.mikepenz.iconics.IconicsDrawable
import fr.o80.beerme.R

/**
 * @author Olivier Perez
 */
class AboutActivity : MaterialAboutActivity() {

    override fun getMaterialAboutList(context: Context): MaterialAboutList {
        val appCard = buildAppCard()
        val developerCard = buildDeveloperCard()
        val designerCard = buildDesignerCard()
        return MaterialAboutList(
                appCard,
                developerCard,
                designerCard
                                )
    }

    private fun buildDeveloperCard(): MaterialAboutCard {
        val builder = MaterialAboutCard.Builder()
        builder.title(R.string.about_developer)
        builder.addItem(MaterialAboutActionItem.Builder()
                                .text("Olivier PEREZ")
                                .subText("France")
                                .icon(icon(GoogleMaterial.Icon.gmd_person))
                                .build())
        builder.addItem(MaterialAboutActionItem.Builder()
                                .text("GitHub")
                                .icon(icon(CommunityMaterial.Icon.cmd_github_circle))
                                .setOnClickListener(ConvenienceBuilder.createWebsiteOnClickAction(this, Uri.parse("https://github.com/olivierperez")))
                                .build())
        builder.addItem(MaterialAboutActionItem.Builder()
                                .text("Twitter")
                                .icon(icon(CommunityMaterial.Icon.cmd_twitter))
                                .setOnClickListener(ConvenienceBuilder.createWebsiteOnClickAction(this, Uri.parse("https://twitter.com/olivierperez")))
                                .build())
        return builder.build()
    }

    private fun buildDesignerCard(): MaterialAboutCard {
        val builder = MaterialAboutCard.Builder()
        builder.title(R.string.about_designer)
        builder.addItem(MaterialAboutActionItem.Builder()
                                .text("...")
                                .icon(icon(GoogleMaterial.Icon.gmd_person))
                                .build())
        return builder.build()
    }

    override fun getActivityTitle(): CharSequence? {
        return getString(R.string.section_info)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                android.R.id.home -> {
                    finish()
                    true
                }
                else -> false
            }

    private fun buildAppCard(): MaterialAboutCard {
        val appCardBuilder = MaterialAboutCard.Builder()

        appCardBuilder.addItem(MaterialAboutActionItem.Builder()
                                       .text(R.string.app_name)
                                       .icon(R.mipmap.ic_launcher)
                                       .showIcon(true)
                                       .build())

        try {
            appCardBuilder.addItem(ConvenienceBuilder.createVersionActionItem(
                    this,
                    icon(GoogleMaterial.Icon.gmd_info_outline),
                    getString(R.string.about_version),
                    false))
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        appCardBuilder.addItem(MaterialAboutActionItem.Builder()
                                       .text("Licenses")
                                       .icon(icon(GoogleMaterial.Icon.gmd_book))
                                       .setOnClickListener {
                                           val intent = Intent(this@AboutActivity, AboutLicenseActivity::class.java)
                                           this@AboutActivity.startActivity(intent)
                                       }
                                       .build())

        return appCardBuilder.build()
    }

    private fun icon(icon: GoogleMaterial.Icon): IconicsDrawable {
        return IconicsDrawable(this)
                .icon(icon)
                .color(ContextCompat.getColor(this, android.R.color.primary_text_light))
                .sizeDp(18)
    }

    private fun icon(icon: CommunityMaterial.Icon): IconicsDrawable {
        return IconicsDrawable(this)
                .icon(icon)
                .color(ContextCompat.getColor(this, android.R.color.primary_text_light))
                .sizeDp(18)
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, AboutActivity::class.java)
    }
}
