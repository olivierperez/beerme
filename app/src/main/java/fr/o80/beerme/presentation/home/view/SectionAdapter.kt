package fr.o80.beerme.presentation.home.view

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import fr.o80.beerme.R

/**
 * @author Olivier Perez
 */
class SectionAdapter(private val sections: List<Section>, private val listener: Listener) : RecyclerView.Adapter<SectionAdapter.SectionViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SectionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_section, parent, false)
        return SectionViewHolder(view)
    }

    override fun onBindViewHolder(holder: SectionViewHolder, position: Int) {
        val section = sections[position]
        holder.bind(section)
        holder.listen(section)
    }

    override fun getItemCount(): Int {
        return sections.size
    }

    data class Section(@param:StringRes @field:StringRes
                           val title: Int, @param:DrawableRes @field:DrawableRes
                           val icon: Int, val open: Action?)

    inner class SectionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.item_section_title)
        lateinit var sectionTitle: TextView

        @BindView(R.id.item_section_ic)
        lateinit var sectionIc: ImageView

        init {
            ButterKnife.bind(this, itemView)
        }

        fun bind(section: Section) {
            sectionTitle.setText(section.title)
            sectionIc.setImageResource(section.icon)
        }

        fun listen(section: Section) {
            itemView.setOnClickListener { listener.onSectionClicked(section) }
        }
    }

    interface Listener {
        fun onSectionClicked(section: Section)
    }

}

typealias Action = () -> Unit
