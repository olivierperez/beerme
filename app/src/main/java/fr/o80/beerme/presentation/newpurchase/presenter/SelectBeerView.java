package fr.o80.beerme.presentation.newpurchase.presenter;

import java.util.List;

import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.PresenterView;

/**
 * @author Olivier Perez
 */
public interface SelectBeerView extends PresenterView {

    void updateBeers(List<Beer> beers);
}
