package fr.o80.beerme.presentation.newpurchase.presenter;

import android.support.annotation.StringRes;

import fr.o80.beerme.data.stock.Stock;
import fr.o80.beerme.presentation.base.PresenterView;

/**
 * @author Olivier Perez
 */
public interface NewStockView extends PresenterView {

    void result(Stock stock);

    void clearErrors();

    void quantityError(@StringRes int messageId);

    void expiryError(@StringRes int messageId);

    void volumeError(@StringRes int messageId);

    void setExpiryDate(String dateStr);
}
