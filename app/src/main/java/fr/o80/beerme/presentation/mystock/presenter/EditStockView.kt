package fr.o80.beerme.presentation.mystock.presenter

import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.presentation.base.PresenterView

/**
 * @author Olivier Perez
 */

interface EditStockView : PresenterView {

    fun displayStock(stock: Stock)
}
