package fr.o80.beerme.presentation.newpurchase.presenter;

import android.support.annotation.StringRes;

import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.PresenterView;

/**
 * @author Olivier Perez
 */
public interface NewBeerView extends PresenterView {
    void goToStock(Beer beer);

    // Errors
    void clearErrors();
    void errorOnName(@StringRes int strRes);
}
