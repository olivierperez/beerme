package fr.o80.beerme.presentation.newpurchase.presenter;

import java.util.List;

import javax.inject.Inject;

import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.Presenter;
import fr.o80.beerme.usecase.ReferenceManagement;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import timber.log.Timber;

/**
 * @author Olivier Perez
 */
public class SelectBeerPresenter extends Presenter<SelectBeerView> {

    @Inject
    protected ReferenceManagement referenceManagement;

    @Inject
    public SelectBeerPresenter() {
    }

    public void init() {
        referenceManagement.all()
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(new DisposableSingleObserver<List<Beer>>() {
                  @Override
                  public void onSuccess(final List<Beer> beers) {
                      if (view == null) {
                          Timber.d("No view attached");
                          return;
                      }

                      view.updateBeers(beers);
                  }

                  @Override
                  public void onError(final Throwable error) {
                      // TODO view.showError(0, 0);
                  }
              });
    }

    public void onDelete(final Beer beer) {
        referenceManagement.delete(beer)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {

                    @Override
                    public void onComplete() {
                        // TODO Juste remove the beer from the list
                        init();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Timber.e(throwable, "Cannot delete beer %s", beer.getName());
                    }
                });
    }
}
