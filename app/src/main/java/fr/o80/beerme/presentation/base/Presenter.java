package fr.o80.beerme.presentation.base;

import io.reactivex.disposables.CompositeDisposable;

/**
 * @author Olivier Perez
 */
public abstract class Presenter<T extends PresenterView> {

    protected T view;

    protected final CompositeDisposable disposables = new CompositeDisposable();

    public void attach(T view) {
        this.view = view;
    }

    public void dettach() {
        view = null;
        disposables.dispose();
    }

}
