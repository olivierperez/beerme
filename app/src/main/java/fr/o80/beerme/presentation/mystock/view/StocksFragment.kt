package fr.o80.beerme.presentation.mystock.view

import android.content.DialogInterface
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import fr.o80.beerme.R
import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.ext.withBundle
import fr.o80.beerme.presentation.base.BaseFragment
import fr.o80.beerme.presentation.common.StockAdapter
import fr.o80.beerme.presentation.mystock.presenter.StocksPresenter
import fr.o80.beerme.presentation.mystock.presenter.StocksView
import fr.o80.beerme.usecase.SoonOutdatedFilter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.fragment_stocks.*
import javax.inject.Inject

/**
 * @author Olivier Perez
 */
class StocksFragment : BaseFragment<StocksPresenter>(), StocksView {

    @Inject
    lateinit var presenter: StocksPresenter

    @Inject
    lateinit var soonOutdatedFilter: SoonOutdatedFilter

    private lateinit var adapter: StockAdapter

    private val searcher = PublishSubject.create<String>()

    private val sort: String
        get() = arguments!![EXTRA_SORT] as String

    /* Fragment */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        component().inject(this)

        adapter = StockAdapter(presenter, soonOutdatedFilter)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.itemAnimator = SlideInUpAnimator(AccelerateDecelerateInterpolator())
    }

    override fun onResume() {
        super.onResume()
        setTitle(R.string.section_stock)
        if (adapter.isEmpty) {
            presenter.load(sort)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.stock, menu)

        val searchView = menu.findItem(R.id.search).actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                searcher.onNext(newText)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.sort -> {
                    presenter.onSortClicked()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    /* StocksView */

    override fun presenter(): StocksPresenter? {
        return presenter
    }

    override fun layoutId(): Int {
        return R.layout.fragment_stocks
    }

    override fun items(items: List<Stock>) {
        errorView.animate().alpha(0f).start()
        recyclerView.animate().alpha(1f).start()
        adapter.addAll(items)
    }

    override fun openEditStock(stock: Stock) {
        EditStockDialogFragment
                .newInstance(stock)
                .show(fragmentManager!!, EditStockDialogFragment.TAG)
    }

    override fun showEmptyMessage(@StringRes strRes: Int) {
        errorMesssage.setText(strRes)
        errorView.visibility = View.VISIBLE
        errorView.animate().alpha(1f).start()
        recyclerView.animate().alpha(0f).start()
    }

    override fun searcher(): Observable<String> {
        return searcher
    }

    fun update() {
        presenter.load(sort)
    }

    override fun showSortOptions(texts: IntArray, checked: Int) {
        val textsString = texts.map(::getString).toTypedArray()
        AlertDialog.Builder(context!!)
                .setTitle(R.string.sort)
                .setSingleChoiceItems(textsString, checked) { dialogInterface: DialogInterface, which: Int ->
                    presenter.onSortSelected(which)
                    dialogInterface.dismiss()
                }
                .show()
    }

    companion object {
        @JvmStatic
        val TAG: String = StocksFragment::class.java.simpleName

        private const val EXTRA_SORT = "EXTRA_SORT"

        fun newInstance(sort: String) =
                StocksFragment().withBundle {
                    putString(EXTRA_SORT, sort)
                }
    }

}
