package fr.o80.beerme.presentation.common;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.util.TypedValue;

/**
 * @author Olivier Perez
 */
public final class ContextHelper {

    private ContextHelper() {
    }

    public static ColorStateList getColor(Context context, @AttrRes int attr) {
        // Get the primary text color of the theme
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
        TypedArray arr = context.obtainStyledAttributes(typedValue.data, new int[]{attr});
        ColorStateList primaryColor = arr.getColorStateList(0);
        arr.recycle();
        return primaryColor;
    }
}
