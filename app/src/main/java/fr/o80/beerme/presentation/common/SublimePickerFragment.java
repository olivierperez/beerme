package fr.o80.beerme.presentation.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;

import fr.o80.beerme.R;

/**
 * @author Olivier Perez
 */
public class SublimePickerFragment extends DialogFragment {

    public static final String EXTRA_SUBLIME_OPTIONS = "EXTRA_SUBLIME_OPTIONS";

    // Callback to activity
    private Callback callback;

    public SublimePickerFragment() {
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    public static SublimePickerFragment newInstance(SublimeOptions options, Callback callback) {

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_SUBLIME_OPTIONS, options);
        SublimePickerFragment fragment = new SublimePickerFragment();
        fragment.setArguments(args);
        fragment.callback = callback;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SublimePicker sublimePicker = (SublimePicker) getActivity()
                .getLayoutInflater().inflate(R.layout.sublime_picker, container);

        // Retrieve SublimeOptions
        SublimeOptions options = getArguments().getParcelable(EXTRA_SUBLIME_OPTIONS);

        sublimePicker.initializePicker(options, new SublimeListenerAdapter() {
            @Override
            public void onCancelled() {
                if (callback != null) {
                    callback.onCancelled(SublimePickerFragment.this);
                }
            }

            @Override
            public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker,
                                                SelectedDate selectedDate,
                                                int hourOfDay, int minute,
                                                SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                                String recurrenceRule) {
                if (callback != null) {
                    callback.onDateTimeRecurrenceSet(
                            SublimePickerFragment.this, selectedDate,
                            hourOfDay, minute, recurrenceOption, recurrenceRule);
                }
            }
        });
        return sublimePicker;
    }

    // For communicating with the activity
    public interface Callback {
        void onCancelled(SublimePickerFragment sublimeFragment);

        void onDateTimeRecurrenceSet(SublimePickerFragment sublimeFragment,
                                     SelectedDate selectedDate,
                                     int hourOfDay, int minute,
                                     SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                     String recurrenceRule);
    }
}
