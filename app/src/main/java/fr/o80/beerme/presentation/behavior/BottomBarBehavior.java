package fr.o80.beerme.presentation.behavior;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;

import timber.log.Timber;

/**
 * @author Olivier Perez
 */
@SuppressWarnings("unused")
public class BottomBarBehavior extends CoordinatorLayout.Behavior<Toolbar> {

    public static final String TAG = BottomBarBehavior.class.getSimpleName();
    public static final int SHORT_DURATION = 200;
    public static final int THRESHOLD = 25;

    private final SnackBarBehavior mSnackBarBehavior;

    private int mOriginalTop = -1;
    private int mStartScroll;
    private int mTotalConsumed;

    public BottomBarBehavior(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        mSnackBarBehavior = new SnackBarBehavior();
    }

    @Override
    public boolean layoutDependsOn(final CoordinatorLayout parent, final Toolbar child, final View dependency) {
        return dependency instanceof RecyclerView || dependency instanceof Snackbar.SnackbarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, Toolbar child, View dependency) {
        if (dependency instanceof Snackbar.SnackbarLayout) {
            mSnackBarBehavior.updateFabTranslationForSnackbar(parent, child);
        }

        return false;
    }

    @Override
    public boolean onStartNestedScroll(final CoordinatorLayout coordinatorLayout, final Toolbar child,
                                       final View directTargetChild, final View target, final int nestedScrollAxes) {
//        Timber.d(TAG, "onStartNestedScroll - target = %s", target);
//        Timber.d(TAG, "onStartNestedScroll - directTargetChild = %s", directTargetChild);

        // Get original top

        if (mOriginalTop == -1) {
            mOriginalTop = child.getTop();
//            Timber.d(TAG, "onStartNestedScroll.mOriginalTop = %d", mOriginalTop);
        }

        mStartScroll = child.getTop();
        mTotalConsumed = 0;

        return true;
    }

    @Override
    public void onNestedScroll(final CoordinatorLayout coordinatorLayout, final Toolbar child, final View target,
                               final int dxConsumed, final int dyConsumed, final int dxUnconsumed, final int dyUnconsumed) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);

//        Timber.d(TAG, "dyConsumed = %d", dyConsumed);
//        Timber.d(TAG, "dyUnconsumed = %d", dyUnconsumed);

        // Collapse/Expand BottomBar

        int newTop = Integer.MIN_VALUE;
        if (dyConsumed != 0) { // Normal scrolling
            newTop = child.getTop() + dyConsumed;
            mTotalConsumed += dyConsumed;

        } else if (dyUnconsumed != 0) { // Overscroll scrolling
            newTop = child.getTop() + dyUnconsumed;
            mTotalConsumed += dyUnconsumed;

        }
        Timber.d(TAG, "onNestedScroll.mTotalConsumed = %d", mTotalConsumed);

        // Apply changes
//        Timber.d(TAG, "newTop = %d", newTop);
        if (newTop > Integer.MIN_VALUE) {
            child.setTop(Math.min(coordinatorLayout.getHeight(), Math.max(mOriginalTop, newTop)));
        }

    }

    @Override
    public void onStopNestedScroll(final CoordinatorLayout coordinatorLayout, final Toolbar child, final View target) {
        int newTop;
        if (mTotalConsumed > THRESHOLD) { // Hide BottomBar
            newTop = coordinatorLayout.getHeight();
        } else if (mTotalConsumed < -THRESHOLD) { // Show BottomBar
            newTop = mOriginalTop;
        } else { // Reset Top
            newTop = mStartScroll;
        }

//        Timber.d(TAG, "onStopNestedScroll (%s)", this);
//        Timber.d(TAG, "onStopNestedScroll.mOriginalTop = %d", mOriginalTop);
//        Timber.d(TAG, "onStopNestedScroll.mTotalConsumed = %d", mTotalConsumed);
//        Timber.d(TAG, "onStopNestedScroll.newTop = %d", newTop);

        ValueAnimator animator = ValueAnimator.ofInt(child.getTop(), newTop);
        animator
              .addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                  @Override
                  public void onAnimationUpdate(final ValueAnimator animation) {
                      child.setTop((Integer) animation.getAnimatedValue());
                  }
              });
        animator
              .setDuration(SHORT_DURATION)
              .start();
    }
}
