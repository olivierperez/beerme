package fr.o80.beerme.presentation.newpurchase.presenter;

import android.text.TextUtils;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.stock.Stock;
import fr.o80.beerme.data.stock.StocksRepository;
import fr.o80.beerme.presentation.base.Presenter;
import timber.log.Timber;

/**
 * @author Olivier Perez
 */
public class NewStockPresenter extends Presenter<NewStockView> {

    private static final DateFormat DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

    @Inject
    StocksRepository mStocksRepository;
    private Beer mBeer;

    @Inject
    public NewStockPresenter() {
    }

    public void init(final Beer beer) {
        mBeer = beer;
    }

    public void onDatePicked(Calendar date) {
        view.setExpiryDate(DATE_FORMAT.format(date.getTime()));
    }

    public void submit(final String quantityStr, final String expiryStr, final String volume) {
        Timber.d("Form sumbited. Quantity: %s, Expiry: %s, Volume: %s", quantityStr, expiryStr, volume);

        // Check form values

        boolean checked = true;

        view.clearErrors();

        // Check Quantity format

        if (!quantityStr.matches("^\\d+$")) {
            checked = false;
            view.quantityError(R.string.error_quantity_format);
        }

        // Check expiry date

        Date expiry;
        try {
            expiry = DATE_FORMAT.parse(expiryStr);
        } catch (ParseException e) {
            expiry = null;
            checked = false;
            view.expiryError(R.string.error_date_format);
        }

        // Check Volume

        if (TextUtils.isEmpty(volume)) {
            checked = false;
            view.volumeError(R.string.error_volume_empty);
        }


        if (checked) {
            // Build Stock object

            Stock stock = new Stock();
            stock.setQuantity(Integer.parseInt(quantityStr));
            stock.setExpiry(expiry);
            stock.setVolume(volume);
            stock.setBeer(mBeer);

            // Send Stock to Activity

            try {
                mStocksRepository.insert(stock);
                view.result(stock);
            } catch (SQLException e) {
                view.showError(0, 0);
            }
        }
    }
}
