package fr.o80.beerme.presentation.mystock.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import fr.o80.beerme.BaseApplication
import fr.o80.beerme.R
import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.presentation.mystock.presenter.EditStockPresenter
import fr.o80.beerme.presentation.mystock.presenter.EditStockView
import org.parceler.Parcels
import javax.inject.Inject

/**
 * @author Olivier Perez
 */
class EditStockDialogFragment : DialogFragment(), EditStockView {

    @Inject
    lateinit var presenter: EditStockPresenter

    private lateinit var listener: EditStockListener

    private lateinit var quantity: TextView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.attach(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? EditStockListener ?: throw IllegalArgumentException("Context must be a EditStockListener")
    }

    override fun onDestroy() {
        presenter.dettach()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (activity!!.application as BaseApplication).component().inject(this)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val stock = Parcels.unwrap<Stock>(arguments!!.getParcelable(STOCK))

        val view = View.inflate(context, R.layout.fragment_edit_stock, null)

        quantity = view.findViewById(R.id.quantity)

        view.findViewById<Button>(R.id.minusOne).setOnClickListener {
            presenter.minus()
        }

        view.findViewById<Button>(R.id.plusOne).setOnClickListener {
            presenter.plus()
        }

        return AlertDialog.Builder(context!!)
                .setTitle(stock.beer.name)
                .setView(view)
                .setPositiveButton(R.string.ok) { _, _ ->
                    presenter.save()
                    listener.updateStocks()
                }
                .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
                .create()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)
    }

    override fun onResume() {
        super.onResume()
        val stock = Parcels.unwrap<Stock>(arguments!!.getParcelable(STOCK))
        presenter.init(stock)
    }

    override fun displayStock(stock: Stock) {
        quantity.text = getString(R.string.quantity_by_volume, stock.quantity, stock.volume)
    }

    override fun showError(@StringRes titleRes: Int, @StringRes messageRes: Int) {
        Toast.makeText(context, "An error occured", Toast.LENGTH_SHORT).show()
    }

    override fun setTitle(@StringRes titleRes: Int) {

    }

    override fun setTitle(title: String) {
        dialog.setTitle(title)
    }

    interface EditStockListener {

        fun updateStocks()
    }

    companion object {

        @JvmStatic
        val TAG: String = EditStockDialogFragment::class.java.simpleName

        private const val STOCK = "STOCK"

        fun newInstance(stock: Stock): EditStockDialogFragment {
            val args = Bundle()
            args.putParcelable(STOCK, Parcels.wrap(stock))
            val fragment = EditStockDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
