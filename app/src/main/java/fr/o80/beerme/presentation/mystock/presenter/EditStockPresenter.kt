package fr.o80.beerme.presentation.mystock.presenter

import java.sql.SQLException

import javax.inject.Inject

import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.data.stock.StocksRepository
import fr.o80.beerme.presentation.base.Presenter
import timber.log.Timber

/**
 * @author Olivier Perez
 */

class EditStockPresenter @Inject
internal constructor(private val stocksRepository: StocksRepository) : Presenter<EditStockView>() {

    private lateinit var stock: Stock

    fun init(stock: Stock) {
        this.stock = stock
        view.displayStock(stock)
    }

    fun minus() {
        if (stock.quantity > 0) {
            stock.quantity = stock.quantity - 1
            view.displayStock(stock)
        }
    }

    fun plus() {
        stock.quantity = stock.quantity + 1
        view.displayStock(stock)
    }

    fun save() {
        try {
            if (stock.quantity > 0) {
                stocksRepository.save(stock)
            } else {
                stocksRepository.delete(stock)
            }
        } catch (e: SQLException) {
            Timber.e(e, "SQLException when updating stock in database.")
            view.showError(0, 0)
        }

    }
}
