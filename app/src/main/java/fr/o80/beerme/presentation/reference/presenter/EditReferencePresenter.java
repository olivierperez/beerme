package fr.o80.beerme.presentation.reference.presenter;

import javax.inject.Inject;

import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.Presenter;
import fr.o80.beerme.usecase.ReferenceManagement;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * @author Olivier Perez
 */
public class EditReferencePresenter extends Presenter<EditReferencePresenterView> {

    @Inject
    protected ReferenceManagement referenceManagement;

    @Inject
    public EditReferencePresenter() {
    }

    public void save(final Beer beer, String name, String brewer, String color, String type, float rate, String comment) {
        boolean error = false;
        view.clearErrors();

        if (name.isEmpty()) {
            view.errorOnName(R.string.new_beer_error_missing_name);
            error = true;
        }

        if (!error) {
            beer.setName(name);
            beer.setBrewer(brewer);
            beer.setColor(color);
            beer.setType(type);
            beer.setRate(rate);
            beer.setComment(comment);
            referenceManagement.save(beer)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new DisposableSingleObserver<Boolean>() {
                        @Override
                        public void onSuccess(Boolean saved) {
                            if (saved) {
                                view.onBeerSaved(beer);
                            } else {
                                view.showError(0, R.string.error_cannot_save_beer);
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            view.showError(0, R.string.error_cannot_save_beer);
                        }
                    });
        }
    }
}
