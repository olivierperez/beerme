package fr.o80.beerme.presentation.base;

import android.support.annotation.StringRes;

/**
 * @author Olivier Perez
 */
public interface PresenterView {

    void showError(@StringRes int titleRes, @StringRes int messageRes);

    void setTitle(@StringRes int titleRes);

    void setTitle(String name);
}
