package fr.o80.beerme.presentation.newpurchase.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.widget.EditText;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.stock.Stock;
import fr.o80.beerme.presentation.base.BaseFragment;
import fr.o80.beerme.presentation.common.SublimePickerFragment;
import fr.o80.beerme.presentation.newpurchase.presenter.NewStockPresenter;
import fr.o80.beerme.presentation.newpurchase.presenter.NewStockView;

/**
 * @author Olivier Perez
 */
public class NewStockFragment extends BaseFragment<NewStockPresenter> implements NewStockView {

    private static final String EXTRA_BEER = "EXTRA_BEER";

    @Inject
    NewStockPresenter presenter;

    @BindView (R.id.quantity)
    EditText quantity;

    @BindView (R.id.expiry)
    EditText expiry;

    @BindView (R.id.volume)
    EditText volume;

    private Beer beer;

    private NewStockListener newStockListener;

    public static NewStockFragment newInstance(final Beer beer) {
        NewStockFragment fragment = new NewStockFragment();

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_BEER, Parcels.wrap(beer));

        fragment.setArguments(args);

        return fragment;
    }

    /* Fragment */

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof NewStockListener) {
            newStockListener = (NewStockListener) context;
        } else {
            throw new RuntimeException("Context must be a NewStockListener");
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        component().inject(this);

        beer = Parcels.unwrap(getArguments().getParcelable(EXTRA_BEER));
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(beer.getName());
        presenter.init(beer);
    }

    /* BaseFragment */

    @Override
    protected NewStockPresenter presenter() {
        return presenter;
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_new_stock;
    }

    /* Listener */

    @OnClick (R.id.new_stock_sumbit)
    public void onSubmitClicked() {
        presenter.submit(
              quantity.getText().toString(),
              expiry.getText().toString(),
              volume.getText().toString()
        );
    }

    @OnFocusChange(R.id.expiry)
    public void onExpiryFocused(boolean focused) {
        if (focused) {
            SublimeOptions options = new SublimeOptions();
            options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
            options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
            options.setCanPickDateRange(false);

            SublimePickerFragment pickerFrag = SublimePickerFragment.newInstance(options, new SublimePickerFragment.Callback() {
                @Override
                public void onCancelled(SublimePickerFragment fragment) {
                    fragment.dismiss();
                }

                @Override
                public void onDateTimeRecurrenceSet(SublimePickerFragment fragment, SelectedDate selectedDate, int hourOfDay, int minute,
                                                    SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {
                    presenter.onDatePicked(selectedDate.getFirstDate());
                    fragment.dismiss();
                }
            });
            pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
        }
    }

    /* NewStockView */

    @Override
    public void result(final Stock stock) {
        newStockListener.onStockAdded(stock);
    }

    @Override
    public void clearErrors() {
        quantity.setError(null);
        expiry.setError(null);
        volume.setError(null);
    }

    @Override
    public void quantityError(@StringRes final int messageId) {
        quantity.setError(getString(messageId));
    }

    @Override
    public void expiryError(@StringRes final int messageId) {
        expiry.setError(getString(messageId));
    }

    @Override
    public void volumeError(@StringRes final int messageId) {
        volume.setError(getString(messageId));
    }

    @Override
    public void setExpiryDate(String dateStr) {
        expiry.setText(dateStr);
    }

    public interface NewStockListener {
        void onStockAdded(Stock stock);
    }
}
