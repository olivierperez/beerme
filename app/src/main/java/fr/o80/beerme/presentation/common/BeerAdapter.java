package fr.o80.beerme.presentation.common;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.data.diff.DiffCallback;
import fr.o80.beerme.common.utils.StringJoiner;

/**
 * @author Olivier Perez
 */
public class BeerAdapter extends RecyclerSwipeAdapter<BeerAdapter.BeerViewHolder> {

    protected final BeerAdapterListener listener;

    private final List<Beer> items = new ArrayList<>();

    public BeerAdapter(final BeerAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public BeerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_beer, parent, false);
        return new BeerViewHolder(view, this.listener);
    }

    @Override
    public void onBindViewHolder(BeerViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public void add(final Beer item) {
        items.add(item);
        notifyItemInserted(items.indexOf(item));
    }

    public void addAll(final List<Beer> newItems) {
        DiffUtil.DiffResult diff = DiffUtil.calculateDiff(new DiffCallback(items, newItems));
        items.clear();
        items.addAll(newItems);
        diff.dispatchUpdatesTo(this);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.item_beer_wipe;
    }

    class BeerViewHolder extends RecyclerView.ViewHolder {

        private final BeerAdapterListener listener;

        @BindView(R.id.beer_item_name)
        protected TextView name;

        @BindView(R.id.beer_item_colortype)
        protected TextView colorType;

        BeerViewHolder(final View itemView, final BeerAdapterListener listener) {
            super(itemView);
            this.listener = listener;
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Beer beer) {
            SwipeLayout swipeLayout = (SwipeLayout) itemView;

            name.setText(beer.getName());
            colorType.setText(StringJoiner.join(" - ", beer.getColor(), beer.getType()));

            swipeLayout.findViewById(R.id.item_beer_surface)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            listener.onItemClicked(beer);
                        }
                    });

            swipeLayout.findViewById(R.id.delete_button)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            listener.onDelete(beer);
                        }
                    });
        }
    }

    /* Inner classes */

    public interface BeerAdapterListener {

        void onItemClicked(Beer beer);

        void onDelete(Beer beer);
    }

}
