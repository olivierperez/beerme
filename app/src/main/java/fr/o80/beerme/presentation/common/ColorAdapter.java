package fr.o80.beerme.presentation.common;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.o80.beerme.R;

/**
 * @author Olivier Perez
 */
public class ColorAdapter extends BaseAdapter implements Filterable {

    @BindView(R.id.colorName)
    TextView colorName;

    private final String[] colors;
    private final Activity activity;

    private Filter mFilter;

    public ColorAdapter(@NonNull Activity activity, @NonNull String[] colors) {
        this.activity = activity;
        this.colors = colors;
    }

    @Override
    public int getCount() {
        return colors.length;
    }

    @Override
    public Object getItem(int position) {
        return colors[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = activity.getLayoutInflater().inflate(R.layout.spinner_color, parent, false);
        } else {
            view = convertView;
        }
        // TODO Use a ViewHolder

        ButterKnife.bind(this, view);
//            BeerColor beerColor = BeerColor.fromPosition(position);

        colorName.setText(colors[position]);
//            colorName.setTextColor(beerColor.getTextColor());
//            view.setBackgroundColor(beerColor.getBackgroundColor());

        return view;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(final CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    results.values = colors;
                    results.count = colors.length;
                    return results;
                }

                @Override
                protected void publishResults(final CharSequence constraint, final FilterResults results) {
                    if (results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
        }

        return mFilter;
    }
}
