package fr.o80.beerme.presentation.mystock.presenter

import android.support.annotation.StringRes

import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.presentation.base.PresenterView
import io.reactivex.Observable

/**
 * @author Olivier Perez
 */
interface StocksView : PresenterView {

    fun items(items: List<Stock>)

    fun openEditStock(stock: Stock)

    fun showEmptyMessage(@StringRes strRes: Int)

    fun searcher(): Observable<String>

    fun showSortOptions(texts: IntArray, checked: Int)
}
