package fr.o80.beerme.presentation.reference.presenter;

import android.support.annotation.StringRes;

import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.PresenterView;

/**
 * @author Olivier Perez
 */
public interface EditReferencePresenterView extends PresenterView {
    void clearErrors();
    void errorOnName(@StringRes int strRes);
    void onBeerSaved(Beer beer);
}
