package fr.o80.beerme.presentation.newpurchase.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.widget.EditText;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import fr.o80.beerme.R;
import fr.o80.beerme.data.beer.Beer;
import fr.o80.beerme.presentation.base.BaseFragment;
import fr.o80.beerme.presentation.common.ColorAdapter;
import fr.o80.beerme.presentation.common.TypeAdapter;
import fr.o80.beerme.presentation.newpurchase.presenter.NewBeerPresenter;
import fr.o80.beerme.presentation.newpurchase.presenter.NewBeerView;

/**
 * @author Olivier Perez
 */
public class NewBeerFragment extends BaseFragment<NewBeerPresenter> implements NewBeerView {

    @Inject
    protected NewBeerPresenter presenter;

    @BindView(R.id.beer_name)
    protected EditText name;

    @BindView(R.id.beer_brewer)
    protected EditText brewer;

    @BindView(R.id.beer_color)
    protected MaterialBetterSpinner color;

    @BindView(R.id.beer_type)
    protected MaterialBetterSpinner type;

    @BindView(R.id.rating_bar)
    protected SimpleRatingBar ratingBar;

    @BindView(R.id.beer_comment)
    protected EditText comment;

    private NewBeerListener newBeerListener;

    public static NewBeerFragment newInstance() {
        return new NewBeerFragment();
    }

    /* Fragment */

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof NewBeerListener) {
            newBeerListener = (NewBeerListener) context;
        } else {
            throw new RuntimeException("Context must be a SelectBeerListener");
        }
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        component().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(R.string.new_reference_title);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Colors
        String[] colors = getResources().getStringArray(R.array.beer_colors);
        color.setAdapter(new ColorAdapter(getActivity(), colors));

        // Types
        String[] types = getResources().getStringArray(R.array.beer_types);
        type.setAdapter(new TypeAdapter(getActivity(), types));
    }

    /* BaseFragment */

    @Override
    protected NewBeerPresenter presenter() {
        return presenter;
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_edit_beer;
    }

    /* Listener */

    @OnClick(R.id.beer_sumbit)
    public void onSubmitClicked() {
        presenter.addBeer(
                name.getText().toString(),
                brewer.getText().toString(),
                color.getText().toString(),
                type.getText().toString(),
                ratingBar.getRating(),
                comment.getText().toString()
        );
    }

    @Override
    public void goToStock(Beer beer) {
        newBeerListener.onNewBeer(beer);
    }

    @Override
    public void clearErrors() {
        name.setError(null);
    }

    @Override
    public void errorOnName(@StringRes int strRes) {
        name.setError(getString(strRes));
    }

    public interface NewBeerListener {
        void onNewBeer(Beer beer);
    }
}
