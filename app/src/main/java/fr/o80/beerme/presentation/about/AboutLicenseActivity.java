package fr.o80.beerme.presentation.about;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;

import com.danielstone.materialaboutlibrary.MaterialAboutActivity;
import com.danielstone.materialaboutlibrary.items.MaterialAboutActionItem;
import com.danielstone.materialaboutlibrary.model.MaterialAboutCard;
import com.danielstone.materialaboutlibrary.model.MaterialAboutList;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.IconicsDrawable;

import fr.o80.beerme.R;

/**
 * @author Olivier Perez
 */
public class AboutLicenseActivity extends MaterialAboutActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setScrollToolbar(true);
    }

    @NonNull
    @Override
    protected MaterialAboutList getMaterialAboutList(@NonNull Context context) {
        MaterialAboutCard androidIconics = lib("Android Iconics", "2016", "Mike Penz", OpenSourceLicense.APACHE_2);
        MaterialAboutCard autoValue = lib("auto", "2013", "Google, Inc.", OpenSourceLicense.APACHE_2);
        MaterialAboutCard betterSpinner = lib("BetterSpinner", "2015", "Wei Wang", OpenSourceLicense.APACHE_2);
        MaterialAboutCard butterKnife = lib("Butter Knife", "2013", "Jake Wharton", OpenSourceLicense.APACHE_2);
        MaterialAboutCard dagger = lib("Dagger 2", "2012", "The Dagger Authors", OpenSourceLicense.APACHE_2);
        MaterialAboutCard daggerMock = lib("DaggerMock", "2016", "Fabio Collini", OpenSourceLicense.APACHE_2);
        MaterialAboutCard materialAbout = lib("material-about-library", "2016", "Daniel Stone", OpenSourceLicense.APACHE_2);
        MaterialAboutCard mockito = lib("mockito", "2007", "Mockito contributors", OpenSourceLicense.MIT);
        MaterialAboutCard ormLite = lib("OrmLite", "2016", "Gray Watson", OpenSourceLicense.ISC);
        MaterialAboutCard parceler = lib("Parceler", "2011-2015", "John Ericksen", OpenSourceLicense.APACHE_2);
        MaterialAboutCard recyclerViewAnimators = lib("RecyclerView Animators", "2017", "Wasabeef", OpenSourceLicense.APACHE_2);
        MaterialAboutCard robolectric = lib("Robolectric", "2010", "Xtreme Labs, Pivotal Labs and Google Inc.", OpenSourceLicense.MIT);
        MaterialAboutCard rxAndroid = lib("RxAndroid: Reactive Extensions for Android", "2015", "The RxAndroid authors", OpenSourceLicense.APACHE_2);
        MaterialAboutCard rxJava = lib("RxJava", "2016", "RxJava Contributors", OpenSourceLicense.APACHE_2);
        MaterialAboutCard simpleRatingBar = lib("SimpleRatingBar", "2016", "Iván Arcuschin", OpenSourceLicense.APACHE_2);
        MaterialAboutCard sublimePicker = lib("SublimePicker", "2015", "vikramkakkar", OpenSourceLicense.APACHE_2);
        MaterialAboutCard timber = lib("Timber", "2013", "Jake Wharton", OpenSourceLicense.APACHE_2);
        MaterialAboutCard timeAgoCard = lib("TimeAgo", "2016", "marlonlom", OpenSourceLicense.APACHE_2);

        return new MaterialAboutList(
                androidIconics,
                autoValue,
                betterSpinner,
                butterKnife,
                dagger,
                daggerMock,
                materialAbout,
                mockito,
                ormLite,
                parceler,
                recyclerViewAnimators,
                robolectric,
                rxAndroid,
                rxJava,
                simpleRatingBar,
                sublimePicker,
                timber,
                timeAgoCard
        );
    }

    private MaterialAboutCard lib(String libraryTitle, String year, String name, OpenSourceLicense apache2) {
        MaterialAboutActionItem licenseItem = new MaterialAboutActionItem.Builder()
                .icon(icon(GoogleMaterial.Icon.gmd_book))
                .setIconGravity(MaterialAboutActionItem.GRAVITY_TOP)
                .text(libraryTitle)
                .subText(String.format(getString(apache2.getResourceId()), year, name))
                .build();

        return new MaterialAboutCard.Builder().addItem(licenseItem).build();
    }

    @Override
    protected CharSequence getActivityTitle() {
        return getString(R.string.section_info);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }

    private IconicsDrawable icon(GoogleMaterial.Icon icon) {
        return new IconicsDrawable(this)
                .icon(icon)
                .color(ContextCompat.getColor(this, android.R.color.primary_text_light))
                .sizeDp(18);
    }

}
