package fr.o80.beerme.presentation.common;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.o80.beerme.R;

/**
 * @author Olivier Perez
 */
public class TypeAdapter extends BaseAdapter implements Filterable {

    @BindView(R.id.typeName)
    TextView typeName;

    private final String[] types;
    private final Activity activity;

    private Filter mFilter;

    public TypeAdapter(@NonNull Activity activity, @NonNull String[] types) {
        this.activity = activity;
        this.types = types;
    }

    @Override
    public int getCount() {
        return types.length;
    }

    @Override
    public Object getItem(int position) {
        return types[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = activity.getLayoutInflater().inflate(R.layout.spinner_type, parent, false);
        } else {
            view = convertView;
        }
        // TODO Use a ViewHolder

        ButterKnife.bind(this, view);

        typeName.setText(types[position]);

        return view;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(final CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    results.values = types;
                    results.count = types.length;
                    return results;
                }

                @Override
                protected void publishResults(final CharSequence constraint, final FilterResults results) {
                    if (results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
        }

        return mFilter;
    }
}
