package fr.o80.beerme.presentation.mystock.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import fr.o80.beerme.R
import fr.o80.beerme.presentation.base.BaseActivity
import fr.o80.beerme.usecase.StockList

/**
 * @author Olivier Perez
 */
class MyStockActivity : BaseActivity(), EditStockDialogFragment.EditStockListener {

    private lateinit var stocksFragment: StocksFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fragment = supportFragmentManager.findFragmentById(R.id.main)

        if (fragment == null) {
            stocksFragment = StocksFragment.newInstance(intent.getStringExtra(EXTRA_SORT))
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main, stocksFragment, StocksFragment.TAG)
                    .commit()
        } else {
            stocksFragment = supportFragmentManager.findFragmentByTag(StocksFragment.TAG) as StocksFragment
        }
    }

    override fun layoutId(): Int {
        return R.layout.activity_simple
    }

    override fun updateStocks() {
        stocksFragment.update()
    }

    companion object {
        private const val EXTRA_SORT = "EXTRA_SORT"

        fun newIntent(context: Context, sort: String) =
                Intent(context, MyStockActivity::class.java).apply {
                    putExtra(EXTRA_SORT, sort)
                }
    }
}
