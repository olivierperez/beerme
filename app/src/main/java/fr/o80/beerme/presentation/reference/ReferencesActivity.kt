package fr.o80.beerme.presentation.reference

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment

import fr.o80.beerme.R
import fr.o80.beerme.data.beer.Beer
import fr.o80.beerme.presentation.base.BaseActivity
import fr.o80.beerme.presentation.mystock.view.StocksFragment
import fr.o80.beerme.presentation.newpurchase.view.SelectBeerFragment
import fr.o80.beerme.presentation.reference.view.EditReferenceFragment

/**
 * @author Olivier Perez
 */
class ReferencesActivity : BaseActivity(), SelectBeerFragment.SelectBeerListener, EditReferenceFragment.EditReferenceListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fragment = supportFragmentManager.findFragmentById(R.id.main)

        if (fragment == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.main, SelectBeerFragment.newInstance(R.string.section_references), StocksFragment.TAG)
                    .commit()
        }
    }

    override fun layoutId(): Int {
        return R.layout.activity_simple
    }

    override fun onBeerClicked(beer: Beer) {
        replaceFragmentWithAnimation(EditReferenceFragment.editBeer(beer), true)
    }

    override fun onNewBeerClicked() {
        replaceFragmentWithAnimation(EditReferenceFragment.newBeer(), true)
    }

    override fun onBeeerSaved(beer: Beer) {
        supportFragmentManager.popBackStack()
    }

    companion object {
        fun newIntent(context: Context) = Intent(context, ReferencesActivity::class.java)
    }
}
