package fr.o80.beerme.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import fr.o80.beerme.BaseApplication;
import fr.o80.beerme.di.AppComponent;

/**
 * @author Olivier Perez
 */
public abstract class BaseFragment<T extends Presenter> extends Fragment implements PresenterView {

    private BaseActivity activity;

    private Unbinder unbinder;

    protected abstract T presenter();

    @LayoutRes
    protected abstract int layoutId();

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);

        if (context instanceof BaseActivity) {
            activity = (BaseActivity) context;
        } else {
            throw new RuntimeException("Context must be a " + BaseActivity.class);
        }
    }

    @Nullable
    @Override
    public final View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(layoutId(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter().attach(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        presenter().dettach();
    }

    protected AppComponent component() {
        return ((BaseApplication) getActivity().getApplication()).component();
    }

    @Override
    public void showError(@StringRes final int titleRes, @StringRes final int messageRes) {
        // TODO Make a nice display for errors
        Toast.makeText(getContext(), getString(messageRes), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setTitle(@StringRes int titleRes) {
        activity.setTitle(titleRes);
    }

    @Override
    public void setTitle(String title) {
        activity.setTitle(title);
    }
}
