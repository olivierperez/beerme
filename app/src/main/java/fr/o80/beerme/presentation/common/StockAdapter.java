package fr.o80.beerme.presentation.common;

import android.support.v4.content.ContextCompat;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.marlonlom.utilities.timeago.TimeAgo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.o80.beerme.R;
import fr.o80.beerme.data.stock.Stock;
import fr.o80.beerme.data.diff.DiffCallback;
import fr.o80.beerme.usecase.SoonOutdatedFilter;
import fr.o80.beerme.common.utils.StringJoiner;

/**
 * @author Olivier Perez
 */
public class StockAdapter extends RecyclerView.Adapter<StockAdapter.StockItemViewHolder> {

    private StockListListener listener;
    private SoonOutdatedFilter soonOutdatedFilter;

    private final List<Stock> items = new ArrayList<>();

    public StockAdapter(final StockListListener listener, SoonOutdatedFilter soonOutdatedFilter) {
        super();
        this.listener = listener;
        this.soonOutdatedFilter = soonOutdatedFilter;
    }

    @Override
    public StockItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StockItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stock, parent, false));
    }

    @Override
    public void onBindViewHolder(StockItemViewHolder holder, int position) {
        Stock stock = items.get(position);
        holder.bind(stock);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void addAll(final List<Stock> stocks) {
        final DiffUtil.DiffResult diff = DiffUtil.calculateDiff(new DiffCallback(items, stocks));
        items.clear();
        items.addAll(stocks);
        diff.dispatchUpdatesTo(StockAdapter.this);
    }

    public boolean isEmpty() {
        return items.size() == 0;
    }

    class StockItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.stock_item_beer_name)
        TextView beerName;

        @BindView(R.id.stock_item_quantity)
        TextView quantity;

        @BindView(R.id.stock_item_beer_type)
        TextView type;

        @BindView(R.id.stock_item_expiry)
        TextView expiry;

        StockItemViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Stock stock) {
            beerName.setText(stock.getBeer().getName());
            quantity.setText(String.format(Locale.getDefault(), "%d x %s", stock.getQuantity(), stock.getVolume()));
            type.setText(StringJoiner.join(" - ", stock.getBeer().getColor(), stock.getBeer().getType()));
            // TODO .replaceAll("''", "'") is a fix for the TimeAgo library
            expiry.setText(TimeAgo.using(stock.getExpiry().getTime()).replaceAll("''", "'"));

            if (soonOutdatedFilter.isAlreadyOutdated(stock)) {
                expiry.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.fct_stock_outdated));
            } else if (soonOutdatedFilter.isSoonOutdated(stock)) {
                expiry.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.fct_stock_soon_outdated));
            } else {
                expiry.setTextColor(ContextHelper.getColor(itemView.getContext(), android.R.attr.textColorTertiary));
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    listener.onStockClicked(stock);
                }
            });
        }

    }

    public interface StockListListener {
        void onStockClicked(Stock stock);
    }

}
