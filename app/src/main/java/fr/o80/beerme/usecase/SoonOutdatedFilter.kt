package fr.o80.beerme.usecase

import java.util.Calendar
import java.util.Date

import fr.o80.beerme.data.stock.Stock

/**
 * @author Olivier Perez
 */
class SoonOutdatedFilter(field: Int, threshold: Int) {

    private val thresholdDate: Date

    init {
        val warningCal = Calendar.getInstance()
        warningCal.add(field, threshold)
        thresholdDate = warningCal.time
    }

    fun isSoonOutdated(stock: Stock): Boolean = thresholdDate.after(stock.expiry)

    fun isAlreadyOutdated(stock: Stock): Boolean = Date().after(stock.expiry)
}
