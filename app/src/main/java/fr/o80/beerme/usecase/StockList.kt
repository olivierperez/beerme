package fr.o80.beerme.usecase

import javax.inject.Inject

import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.data.stock.StocksRepository
import fr.o80.beerme.service.QueryFilter
import io.reactivex.Single
import io.reactivex.functions.Predicate
import io.reactivex.schedulers.Schedulers

/**
 * @author Olivier Perez
 */
class StockList @Inject
constructor(private val stocksRepository: StocksRepository, private val queryFilter: QueryFilter) {

    fun all(sortType: String): Single<List<Stock>> {
        return stocksRepository
                .all()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .toSortedList(when (sortType) {
                                  SORT_BY_EXPIRY -> Comparator { stock1, stock2 ->
                                      // First compare expiry dates, then names
                                      val expiryCompared = stock1.expiry.compareTo(stock2.expiry)
                                      when (expiryCompared) {
                                          0 -> stock1.beer.name.compareTo(stock2.beer.name)
                                          else -> expiryCompared
                                      }
                                  }
                                  else -> Comparator<Stock> { stock1, stock2 ->
                                      // First compare names, then expiry dates
                                      val beerCompared = stock1.beer.name.compareTo(stock2.beer.name)
                                      when (beerCompared) {
                                          0 -> stock1.expiry.compareTo(stock2.expiry)
                                          else -> beerCompared
                                      }
                                  }
                              })
    }

    fun filter(query: String): Predicate<Stock> {
        return Predicate { stock ->
            queryFilter.matches(query,
                                stock.beer.name,
                                stock.beer.color,
                                stock.beer.type)
        }
    }

    companion object {
        const val SORT_BY_BEER = "SORT_BY_BEER"
        const val SORT_BY_EXPIRY = "SORT_BY_EXPIRY"
    }

}
