package fr.o80.beerme.usecase

import fr.o80.beerme.data.stock.Stock
import fr.o80.beerme.data.stock.StocksRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Olivier Perez
 */
class SoonOutdated @Inject
constructor(private val stocksRepository: StocksRepository, private val soonOutdatedFilter: SoonOutdatedFilter) {

    val count: Single<CountOutdated>
        get() = stocksRepository.all()
                .reduce(CountOutdated(0, 0)) { pair, stock ->
                    when {
                        soonOutdatedFilter.isAlreadyOutdated(stock) -> pair.apply(CountOutdated::incrementOutdated)
                        soonOutdatedFilter.isSoonOutdated(stock) -> pair.apply(CountOutdated::incrementSoonOutdated)
                        else -> pair
                    }
                }

    fun get(): Single<List<Stock>> =
            stocksRepository.all()
                    .filter { stock -> soonOutdatedFilter.isSoonOutdated(stock) }
                    .toList()

    data class CountOutdated(var outdated: Int, var soonOutdated: Int) {
        fun incrementOutdated() {
            outdated++
        }

        fun incrementSoonOutdated() {
            soonOutdated++
        }
    }
}
