package fr.o80.beerme.usecase

import fr.o80.beerme.data.beer.Beer
import fr.o80.beerme.data.beer.BeerRepository
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author Olivier Perez
 */
@Singleton
class ReferenceManagement @Inject
constructor(private val beerRepository: BeerRepository) {

    fun all(): Single<List<Beer>> =
            beerRepository.all()
                    .map { beers ->
                        beers.sortBy(Beer::getName)
                        beers
                    }

    fun save(beer: Beer): Single<Boolean> =
            if (beer.id != 0) {
                beerRepository.save(beer)
            } else {
                beerRepository.insert(beer)
                        .map { true }
            }

    fun delete(beer: Beer): Completable = beerRepository.delete(beer)

}
