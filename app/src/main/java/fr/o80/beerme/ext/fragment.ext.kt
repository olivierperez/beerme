package fr.o80.beerme.ext

import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * @author Olivier Perez
 */
inline fun <F : Fragment> F.withBundle(block: Bundle.() -> Unit): F = apply {
    arguments = android.os.Bundle().apply(block)
}