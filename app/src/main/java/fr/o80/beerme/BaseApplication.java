package fr.o80.beerme;

import android.app.Application;

import fr.o80.beerme.di.AppComponent;
import fr.o80.beerme.di.DaggerAppComponent;
import fr.o80.beerme.di.RepositoryModule;
import fr.o80.beerme.di.StockModule;
import timber.log.Timber;

/**
 * @author Olivier Perez
 */
public abstract class BaseApplication extends Application {

    private AppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initTimber();
        initDagger();
    }

    protected void initTimber() {
        Timber.uprootAll();
    }

    private void initDagger() {
        mComponent = DaggerAppComponent.builder()
                .repositoryModule(new RepositoryModule(this))
                .stockModule(new StockModule())
                .build();
    }

    public AppComponent component() {
        return mComponent;
    }
}
