package fr.o80.beerme.service;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Olivier Perez
 */
@Singleton
public class UserPrefs {

    private static final String SHARE_PREF_USER = "UserPrefs";

    private static final String PREF_SOON_OUTDATED_LIMIT = "a";
    private static final String PREF_SOON_OUTDATED_FIELD = "b";

    private SharedPreferences sharedPreferences;

    @Inject
    public UserPrefs(Context ctx) {
        sharedPreferences = ctx.getSharedPreferences(SHARE_PREF_USER, Context.MODE_PRIVATE);
    }

    public int getSoonOutdatedLimitInDays() {
        return sharedPreferences.getInt(PREF_SOON_OUTDATED_LIMIT, 30);
    }

    public int getSoonOutdatedField() {
        return sharedPreferences.getInt(PREF_SOON_OUTDATED_FIELD, Calendar.DAY_OF_YEAR);
    }
}
