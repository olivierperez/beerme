package fr.o80.beerme.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import fr.o80.beerme.BeerApplication;
import fr.o80.beerme.BuildConfig;
import timber.log.Timber;

/**
 * @author Olivier Perez
 */
public class OnStartupReceiver extends BroadcastReceiver {

    @Inject
    protected AlarmService alarmService;

    @Override
    public void onReceive(Context context, Intent intent) {
        Timber.d("OnStartupReceiver");
        ((BeerApplication) context.getApplicationContext()).component().inject(this);

        if (BuildConfig.DEBUG || Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            // TODO Get the start date + repeating config from SharedPrefs
            alarmService.startAlarm(context);
        }
    }
}
