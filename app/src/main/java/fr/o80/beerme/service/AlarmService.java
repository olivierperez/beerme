package fr.o80.beerme.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;

import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Singleton;

import fr.o80.beerme.BuildConfig;
import timber.log.Timber;

/**
 * @author Olivier Perez
 */
@Singleton
public class AlarmService {

    private static final int ONE_DAY_IN_MILLIS = 86400 * 1000;
    private static final int ONE_MINUTE_IN_MILLIS = 60 * 1000;

    @Inject
    public AlarmService() {
    }

    public void startAlarm(Context context) {
        Timber.d("startAlarm");

        // Prepare intent that starts the BroadcastReceiver
        PendingIntent pendingIntent = SoonOutdatedReceiver.newInstance(context, PendingIntent.FLAG_UPDATE_CURRENT);

        // Start the alarm
        // TODO Do it better, like providing it into a Dagger module
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (BuildConfig.DEBUG) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + 2000, ONE_MINUTE_IN_MILLIS, pendingIntent);
        } else {
            // Set the alarm to start at 19:00 p.m.
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 19);
            calendar.set(Calendar.MINUTE, 0);

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), ONE_DAY_IN_MILLIS, pendingIntent);
        }

        Timber.d("startAlarm end");
    }
}
