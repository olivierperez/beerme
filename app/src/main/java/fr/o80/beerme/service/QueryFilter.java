package fr.o80.beerme.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * @author Olivier Perez
 */
public class QueryFilter {

    public static final String SPLIT_PATTERN = "[\\s,:]";

    @Inject
    public QueryFilter() {
    }

    /**
     * Check if one of the words matches the query.
     *
     * @param query  The query to match
     * @param values The words
     * @return {@code true} if, at least, one word matches.
     */
    public boolean matches(String query, String... values) {
        if (query == null || query.isEmpty()) {
            return true;
        }

        List<String> words = split(values);
        List<String> queries = split(query);

        for (String q : queries) {
            if (q != null && !q.isEmpty()) {
                if (!matchesAtLeastOnTime(words, q)) {
                    return false;
                }
            }
        }
        return true;
    }

    private List<String> split(String... values) {
        List<String> result = new ArrayList<>();
        for (String value : values) {
            Collections.addAll(result, value.toLowerCase(Locale.getDefault()).split(SPLIT_PATTERN));
        }
        return result;
    }

    private boolean matchesAtLeastOnTime(List<String> words, String q) {
        for (String word : words) {
            if (word != null && !word.isEmpty()) {
                if (word.trim().matches(q + ".*")) {
                    return true;
                }
            }
        }
        return false;
    }
}
