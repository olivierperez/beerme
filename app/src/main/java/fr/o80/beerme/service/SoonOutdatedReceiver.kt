package fr.o80.beerme.service

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.support.v4.app.TaskStackBuilder
import fr.o80.beerme.BeerApplication
import fr.o80.beerme.Const
import fr.o80.beerme.R
import fr.o80.beerme.ext.notification
import fr.o80.beerme.presentation.mystock.view.MyStockActivity
import fr.o80.beerme.usecase.SoonOutdated
import fr.o80.beerme.usecase.StockList
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

/**
 * @author Olivier Perez
 */
class SoonOutdatedReceiver : BroadcastReceiver() {

    @Inject
    lateinit var soonOutdated: SoonOutdated

    override fun onReceive(context: Context, intent: Intent) {
        Timber.d("SoonOutdatedReceiver")
        (context.applicationContext as BeerApplication).component().inject(this)

        // List all stock that are soon outdated
        soonOutdated.count
                .subscribeBy(
                        onSuccess = { count ->
                            // Open notification if there are some
                            if (count.outdated > 0 || count.soonOutdated > 0) {
                                showNotification(context, count.outdated, count.soonOutdated)
                            }
                        },
                        onError = { error ->
                            Timber.e(error, "Failed to load stock that is soon outdated")
                        })

    }

    private fun showNotification(context: Context, outdatedStock: Int, soonOutdatedStock: Int) {
        notification(context) {
            channel("EXPIRY") {
                group("STOCK", context.getString(R.string.notification_stock_groupname))

                name = context.getString(R.string.notification_expiry_channelname)
                description = context.getString(R.string.notification_expiry_channeldescription)
                lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
                importance = NotificationManager.IMPORTANCE_LOW
                lights = false
                vibration = false
                bypassDnd = false
            }


            id = 0
            smallIcon = R.drawable.ic_beer
            autoCancel = true
            priority = NotificationCompat.PRIORITY_DEFAULT
            title = context.getString(R.string.outdated_notif_title)
            intent = TaskStackBuilder.create(context)
                    .addParentStack(MyStockActivity::class.java)
                    .addNextIntent(MyStockActivity.newIntent(context, StockList.SORT_BY_EXPIRY))
                    .getPendingIntent(Const.REQ_CODE_SOON_OUTDATED_NOTIFICATION, PendingIntent.FLAG_CANCEL_CURRENT)

            if (outdatedStock > 0 && soonOutdatedStock > 0) {
                text = context.getString(R.string.interresting_notif_content, outdatedStock + soonOutdatedStock)
                val alreadyOutdatedText = context.resources.getQuantityString(R.plurals.already_outdated_notif_content, outdatedStock, outdatedStock)
                val soonOutdatedText = context.resources.getQuantityString(R.plurals.soon_outdated_notif_content, soonOutdatedStock, soonOutdatedStock)
                style = bigText("$alreadyOutdatedText\n$soonOutdatedText")
            } else {
                if (outdatedStock > 0) {
                    text = context.resources.getQuantityString(R.plurals.already_outdated_notif_content, outdatedStock, outdatedStock)
                } else if (soonOutdatedStock > 0) {
                    text = context.resources.getQuantityString(R.plurals.soon_outdated_notif_content, outdatedStock, outdatedStock)
                }
            }

        }
    }

    companion object {
        @JvmStatic
        fun newInstance(context: Context, flags: Int): PendingIntent? {
            val intent = Intent(context, SoonOutdatedReceiver::class.java)
            return PendingIntent.getBroadcast(context, 0, intent, flags)
        }
    }
}
